#ifndef CMOS_HH
#define CMOS_HH

struct CMOS: public skel::CMOS
{
  CMOS(const dzn::locator& l)
  : skel::CMOS(l)
  {}
  virtual void port_prepare () {std::clog << "prepare\n";}
  virtual void port_acquire () {std::clog << "acquire\n";}
  virtual void port_cancel ()  {std::clog << "cancel\n";}
};

#endif // CMOS_HH
