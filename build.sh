#! /usr/bin/env bash

set -ex

# customize these for your environment:
CXX=g++
CPPFLAGS="-I . -I runtime -I foo/src -I bar/src -I baz/src"
CXXFLAGS="-pthread -std=c++11 -g -O2 -Wall"

# clean
rm -fr build

# build
mkdir -p build
$CXX $CPPFLAGS $CXXFLAGS -o build/camera foo/src/*.cc bar/src/*.cc baz/src/*.cc main.cc runtime/*.cc
