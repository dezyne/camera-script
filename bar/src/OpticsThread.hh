#include <dzn/meta.hh>

namespace dzn {
  struct locator;
  struct runtime;
}



#include <iostream>
#include <map>


/********************************** INTERFACE *********************************/
#ifndef ILENS_HH
#define ILENS_HH



struct ILens
{

  struct
  {
    std::function< void()> forward;
    std::function< void()> backward;
    std::function< void()> stop;
  } in;

  struct
  {
    std::function< void()> stopped;
  } out;

  dzn::port::meta meta;
  inline ILens(const dzn::port::meta& m) : meta(m) {}

  void check_bindings() const
  {
    if (! in.forward) throw dzn::binding_error(meta, "in.forward");
    if (! in.backward) throw dzn::binding_error(meta, "in.backward");
    if (! in.stop) throw dzn::binding_error(meta, "in.stop");

    if (! out.stopped) throw dzn::binding_error(meta, "out.stopped");

  }
};

inline void connect (ILens& provided, ILens& required)
{
  provided.out = required.out;
  required.in = provided.in;
  provided.meta.requires = required.meta.requires;
  required.meta.provides = provided.meta.provides;
}





#endif // ILENS_HH

/********************************** INTERFACE *********************************/
/********************************** INTERFACE *********************************/
#ifndef IFOCUS_HH
#define IFOCUS_HH



struct IFocus
{

  struct
  {
    std::function< void()> measure;
    std::function< void()> cancel;
  } in;

  struct
  {
    std::function< void()> maximum;
  } out;

  dzn::port::meta meta;
  inline IFocus(const dzn::port::meta& m) : meta(m) {}

  void check_bindings() const
  {
    if (! in.measure) throw dzn::binding_error(meta, "in.measure");
    if (! in.cancel) throw dzn::binding_error(meta, "in.cancel");

    if (! out.maximum) throw dzn::binding_error(meta, "out.maximum");

  }
};

inline void connect (IFocus& provided, IFocus& required)
{
  provided.out = required.out;
  required.in = provided.in;
  provided.meta.requires = required.meta.requires;
  required.meta.provides = provided.meta.provides;
}





#endif // IFOCUS_HH

/********************************** INTERFACE *********************************/
/********************************** INTERFACE *********************************/
#ifndef ICONTRAST_HH
#define ICONTRAST_HH



struct IContrast
{
#ifndef ENUM_IContrast_EContrast
#define ENUM_IContrast_EContrast 1


  struct EContrast
  {
    enum type
    {
      Sharper,Blurrier
    };
  };


#endif // ENUM_IContrast_EContrast

  struct
  {
    std::function< ::IContrast::EContrast::type()> measure;
  } in;

  struct
  {
  } out;

  dzn::port::meta meta;
  inline IContrast(const dzn::port::meta& m) : meta(m) {}

  void check_bindings() const
  {
    if (! in.measure) throw dzn::binding_error(meta, "in.measure");


  }
};

inline void connect (IContrast& provided, IContrast& required)
{
  provided.out = required.out;
  required.in = provided.in;
  provided.meta.requires = required.meta.requires;
  required.meta.provides = provided.meta.provides;
}


#ifndef ENUM_TO_STRING_IContrast_EContrast
#define ENUM_TO_STRING_IContrast_EContrast 1
inline std::string to_string(::IContrast::EContrast::type v)
{
  switch(v)
  {
    case ::IContrast::EContrast::Sharper: return "EContrast_Sharper";
    case ::IContrast::EContrast::Blurrier: return "EContrast_Blurrier";

  }
  return "";
}
#endif // ENUM_TO_STRING_IContrast_EContrast

#ifndef STRING_TO_ENUM_IContrast_EContrast
#define STRING_TO_ENUM_IContrast_EContrast 1
inline ::IContrast::EContrast::type to_IContrast_EContrast(std::string s)
{
  static std::map<std::string, ::IContrast::EContrast::type> m = {
    {"EContrast_Sharper", ::IContrast::EContrast::Sharper},
    {"EContrast_Blurrier", ::IContrast::EContrast::Blurrier},
  };
  return m.at(s);
}
#endif // STRING_TO_ENUM_IContrast_EContrast


#endif // ICONTRAST_HH

/********************************** INTERFACE *********************************/
/********************************** INTERFACE *********************************/
#ifndef ISHUTTER_HH
#define ISHUTTER_HH



struct IShutter
{

  struct
  {
    std::function< void()> expose;
  } in;

  struct
  {
  } out;

  dzn::port::meta meta;
  inline IShutter(const dzn::port::meta& m) : meta(m) {}

  void check_bindings() const
  {
    if (! in.expose) throw dzn::binding_error(meta, "in.expose");


  }
};

inline void connect (IShutter& provided, IShutter& required)
{
  provided.out = required.out;
  required.in = provided.in;
  provided.meta.requires = required.meta.requires;
  required.meta.provides = provided.meta.provides;
}





#endif // ISHUTTER_HH

/********************************** INTERFACE *********************************/
/***********************************  FOREIGN  **********************************/
#ifndef SKEL_LENS_HH
#define SKEL_LENS_HH

#include <dzn/locator.hh>
#include <dzn/runtime.hh>




namespace skel {
  struct Lens
  {
    dzn::meta dzn_meta;
    dzn::runtime& dzn_rt;
    dzn::locator const& dzn_locator;
    ::ILens port;


    Lens(const dzn::locator& dzn_locator)
    : dzn_meta{"","Lens",0,0,{},{},{[this]{port.check_bindings();}}}
    , dzn_rt(dzn_locator.get<dzn::runtime>())
    , dzn_locator(dzn_locator)

    , port({{"port",this,&dzn_meta},{"",0,0}})


    {
      port.in.forward = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_forward();}, this->port.meta, "forward");};
      port.in.backward = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_backward();}, this->port.meta, "backward");};
      port.in.stop = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_stop();}, this->port.meta, "stop");};


    }
    virtual ~ Lens() {}
    virtual std::ostream& stream_members(std::ostream& os) const { return os; }
    void check_bindings() const;
    void dump_tree(std::ostream& os) const;
    void set_state(std::map<std::string,std::map<std::string,std::string> >){}
    void set_state(std::map<std::string,std::string>_alist){}
    friend std::ostream& operator << (std::ostream& os, const Lens& m)  {
      return m.stream_members(os);
    }
    private:
    virtual void port_forward () = 0;
    virtual void port_backward () = 0;
    virtual void port_stop () = 0;

  };
}

#endif // LENS_HH

/***********************************  FOREIGN  **********************************/
/***********************************  FOREIGN  **********************************/
#ifndef SKEL_CONTRAST_HH
#define SKEL_CONTRAST_HH

#include <dzn/locator.hh>
#include <dzn/runtime.hh>




namespace skel {
  struct Contrast
  {
    dzn::meta dzn_meta;
    dzn::runtime& dzn_rt;
    dzn::locator const& dzn_locator;
    ::IContrast port;


    Contrast(const dzn::locator& dzn_locator)
    : dzn_meta{"","Contrast",0,0,{},{},{[this]{port.check_bindings();}}}
    , dzn_rt(dzn_locator.get<dzn::runtime>())
    , dzn_locator(dzn_locator)

    , port({{"port",this,&dzn_meta},{"",0,0}})


    {

      port.in.measure = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_measure();}, this->port.meta, "measure");};

    }
    virtual ~ Contrast() {}
    virtual std::ostream& stream_members(std::ostream& os) const { return os; }
    void check_bindings() const;
    void dump_tree(std::ostream& os) const;
    void set_state(std::map<std::string,std::map<std::string,std::string> >){}
    void set_state(std::map<std::string,std::string>_alist){}
    friend std::ostream& operator << (std::ostream& os, const Contrast& m)  {
      return m.stream_members(os);
    }
    private:
    virtual ::IContrast::EContrast::type port_measure () = 0;

  };
}

#endif // CONTRAST_HH

/***********************************  FOREIGN  **********************************/
/********************************** COMPONENT *********************************/
#ifndef FOCUSCONTROL_HH
#define FOCUSCONTROL_HH




struct FocusControl
{
  dzn::meta dzn_meta;
  dzn::runtime& dzn_rt;
  dzn::locator const& dzn_locator;

  bool idle;
  ::IContrast::EContrast::type contrast_was;

  ::IContrast::EContrast::type reply_IContrast_EContrast;

  std::function<void ()> out_port;

  ::IFocus port;

  ::ILens lens;
  ::IContrast sensor;


  FocusControl(const dzn::locator&);
  void check_bindings() const;
  void dump_tree(std::ostream& os) const;
  friend std::ostream& operator << (std::ostream& os, const FocusControl& m)  {
    (void)m;
    return os << "[" << m.idle <<", " << m.contrast_was <<"]" ;
  }
  private:
  void port_measure();
  void port_cancel();
  void lens_stopped();

};

#endif // FOCUSCONTROL_HH

/********************************** COMPONENT *********************************/
/***********************************  FOREIGN  **********************************/
#ifndef SKEL_SHUTTER_HH
#define SKEL_SHUTTER_HH

#include <dzn/locator.hh>
#include <dzn/runtime.hh>




namespace skel {
  struct Shutter
  {
    dzn::meta dzn_meta;
    dzn::runtime& dzn_rt;
    dzn::locator const& dzn_locator;
    ::IShutter port;


    Shutter(const dzn::locator& dzn_locator)
    : dzn_meta{"","Shutter",0,0,{},{},{[this]{port.check_bindings();}}}
    , dzn_rt(dzn_locator.get<dzn::runtime>())
    , dzn_locator(dzn_locator)

    , port({{"port",this,&dzn_meta},{"",0,0}})


    {
      port.in.expose = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_expose();}, this->port.meta, "expose");};


    }
    virtual ~ Shutter() {}
    virtual std::ostream& stream_members(std::ostream& os) const { return os; }
    void check_bindings() const;
    void dump_tree(std::ostream& os) const;
    void set_state(std::map<std::string,std::map<std::string,std::string> >){}
    void set_state(std::map<std::string,std::string>_alist){}
    friend std::ostream& operator << (std::ostream& os, const Shutter& m)  {
      return m.stream_members(os);
    }
    private:
    virtual void port_expose () = 0;

  };
}

#endif // SHUTTER_HH

/***********************************  FOREIGN  **********************************/
/********************************** COMPONENT *********************************/
#ifndef OPTICSCONTROL_HH
#define OPTICSCONTROL_HH

#include "IOptics.hh"



struct OpticsControl
{
  dzn::meta dzn_meta;
  dzn::runtime& dzn_rt;
  dzn::locator const& dzn_locator;
#ifndef ENUM_OpticsControl_State
#define ENUM_OpticsControl_State 1


  struct State
  {
    enum type
    {
      Idle,Prepare,Ready
    };
  };


#endif // ENUM_OpticsControl_State

  ::OpticsControl::State::type state;
  bool ready;


  std::function<void ()> out_port;

  ::IOptics port;

  ::IFocus focus;
  ::IShutter shutter;


  OpticsControl(const dzn::locator&);
  void check_bindings() const;
  void dump_tree(std::ostream& os) const;
  friend std::ostream& operator << (std::ostream& os, const OpticsControl& m)  {
    (void)m;
    return os << "[" << m.state <<", " << m.ready <<"]" ;
  }
  private:
  void port_prepare();
  void port_capture();
  void port_cancel();
  void focus_maximum();

};

#endif // OPTICSCONTROL_HH

/********************************** COMPONENT *********************************/
/***********************************  SYSTEM  ***********************************/
#ifndef FOCUS_HH
#define FOCUS_HH


#include <dzn/locator.hh>

#include "Contrast.hh"
#include "Lens.hh"



struct Focus
{
  dzn::meta dzn_meta;
  dzn::runtime& dzn_rt;
  dzn::locator const& dzn_locator;


  ::FocusControl focus;
  ::Contrast sensor;
  ::Lens lens;

  ::IFocus& port;


  Focus(const dzn::locator&);
  void check_bindings() const;
  void dump_tree(std::ostream& os=std::clog) const;
};

#endif // FOCUS_HH

/***********************************  SYSTEM  ***********************************/
/***********************************  SHELL  ************************************/
#ifndef OPTICSTHREAD_HH
#define OPTICSTHREAD_HH


#include <dzn/locator.hh>
#include <dzn/runtime.hh>
#include <dzn/pump.hh>

#include "Shutter.hh"



struct OpticsThread
{
  dzn::meta dzn_meta;
  dzn::runtime dzn_rt;
  dzn::locator dzn_locator;


  ::OpticsControl optics;
  ::Shutter shutter;
  ::Focus focus;

  ::IOptics port;


  dzn::pump dzn_pump;
  OpticsThread(const dzn::locator&);
  void check_bindings() const;
  void dump_tree(std::ostream& os=std::clog) const;
};

#endif // OPTICSTHREAD_HH

/***********************************  SHELL  ************************************/


//version: 2.9.1
