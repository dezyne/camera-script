#include "OpticsThread.hh"

#include <dzn/locator.hh>
#include <dzn/runtime.hh>



FocusControl::FocusControl(const dzn::locator& dzn_locator)
: dzn_meta{"","FocusControl",0,0,{& lens.meta,& sensor.meta},{},{[this]{port.check_bindings();},[this]{lens.check_bindings();},[this]{sensor.check_bindings();}}}
, dzn_rt(dzn_locator.get<dzn::runtime>())
, dzn_locator(dzn_locator)
, idle(true), contrast_was(::IContrast::EContrast::Blurrier)

, port({{"port",this,&dzn_meta},{"",0,0}})

, lens({{"",0,0},{"lens",this,&dzn_meta}})
, sensor({{"",0,0},{"sensor",this,&dzn_meta}})


{
  dzn_rt.performs_flush(this) = true;

  port.in.measure = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_measure();}, this->port.meta, "measure");};
  port.in.cancel = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_cancel();}, this->port.meta, "cancel");};
  lens.out.stopped = [&](){return dzn::call_out(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->lens) = false; return lens_stopped();}, this->lens.meta, "stopped");};





}

void FocusControl::port_measure()
{
  if (idle) 
  {
    idle = false;
    contrast_was = this->sensor.in.measure();
    this->lens.in.forward();
  }
  else if (!(idle)) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void FocusControl::port_cancel()
{

  {
    idle = true;
    this->lens.in.stop();
  }

  return;

}
void FocusControl::lens_stopped()
{
  if (!(idle)) 
  {
    ::IContrast::EContrast::type contrast_is = this->sensor.in.measure();
    {
      if ((contrast_was == ::IContrast::EContrast::Sharper && contrast_is == ::IContrast::EContrast::Blurrier)) 
      {
        idle = true;
        this->port.out.maximum();
      }
      else {
        if ((contrast_was == ::IContrast::EContrast::Sharper && contrast_is == ::IContrast::EContrast::Sharper)) this->lens.in.forward();
        else this->lens.in.backward();
      }
    }
    contrast_was = contrast_is;
  }
  else if (!(!(idle))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}


void FocusControl::check_bindings() const
{
  dzn::check_bindings(&dzn_meta);
}
void FocusControl::dump_tree(std::ostream& os) const
{
  dzn::dump_tree(os, &dzn_meta);
}


OpticsControl::OpticsControl(const dzn::locator& dzn_locator)
: dzn_meta{"","OpticsControl",0,0,{& focus.meta,& shutter.meta},{},{[this]{port.check_bindings();},[this]{focus.check_bindings();},[this]{shutter.check_bindings();}}}
, dzn_rt(dzn_locator.get<dzn::runtime>())
, dzn_locator(dzn_locator)
, state(::OpticsControl::State::Idle), ready(false)

, port({{"port",this,&dzn_meta},{"",0,0}})

, focus({{"",0,0},{"focus",this,&dzn_meta}})
, shutter({{"",0,0},{"shutter",this,&dzn_meta}})


{
  dzn_rt.performs_flush(this) = true;

  port.in.prepare = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_prepare();}, this->port.meta, "prepare");};
  port.in.capture = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_capture();}, this->port.meta, "capture");};
  port.in.cancel = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_cancel();}, this->port.meta, "cancel");};
  focus.out.maximum = [&](){return dzn::call_out(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->focus) = false; return focus_maximum();}, this->focus.meta, "maximum");};





}

void OpticsControl::port_prepare()
{
  if (state == ::OpticsControl::State::Idle) 
  {
    state = ::OpticsControl::State::Prepare;
    this->focus.in.measure();
  }
  else if (!(state == ::OpticsControl::State::Idle)) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void OpticsControl::port_capture()
{
  if (state == ::OpticsControl::State::Prepare) 
  {
    state = ::OpticsControl::State::Idle;
    this->focus.in.cancel();
    this->shutter.in.expose();
  }
  else if (state == ::OpticsControl::State::Ready) 
  {
    state = ::OpticsControl::State::Idle;
    this->shutter.in.expose();
  }
  else if ((!(state == ::OpticsControl::State::Ready) && !(state == ::OpticsControl::State::Prepare))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void OpticsControl::port_cancel()
{

  {
    this->focus.in.cancel();
    state = ::OpticsControl::State::Idle;
  }

  return;

}
void OpticsControl::focus_maximum()
{
  if (state == ::OpticsControl::State::Prepare) 
  {
    state = ::OpticsControl::State::Ready;
    this->port.out.ready();
  }
  else if (!(state == ::OpticsControl::State::Prepare)) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}


void OpticsControl::check_bindings() const
{
  dzn::check_bindings(&dzn_meta);
}
void OpticsControl::dump_tree(std::ostream& os) const
{
  dzn::dump_tree(os, &dzn_meta);
}

//SYSTEM

Focus::Focus(const dzn::locator& dzn_locator)
: dzn_meta{"","Focus",0,0,{},{& focus.dzn_meta,& sensor.dzn_meta,& lens.dzn_meta},{[this]{port.check_bindings();}}}
, dzn_rt(dzn_locator.get<dzn::runtime>())
, dzn_locator(dzn_locator)


, focus(dzn_locator)
, sensor(dzn_locator)
, lens(dzn_locator)

, port(focus.port)

{


  focus.dzn_meta.parent = &dzn_meta;
  focus.dzn_meta.name = "focus";
  sensor.dzn_meta.parent = &dzn_meta;
  sensor.dzn_meta.name = "sensor";
  lens.dzn_meta.parent = &dzn_meta;
  lens.dzn_meta.name = "lens";


  connect(lens.port, focus.lens);
  connect(sensor.port, focus.sensor);

  dzn::rank(port.meta.provides.meta, 0);

}

void Focus::check_bindings() const
{
  dzn::check_bindings(&dzn_meta);
}
void Focus::dump_tree(std::ostream& os) const
{
  dzn::dump_tree(os, &dzn_meta);
}

////////////////////////////////////////////////////////////////////////////////

OpticsThread::OpticsThread(const dzn::locator& locator)
: dzn_meta{"","OpticsThread",0,0,{},{& optics.dzn_meta,& shutter.dzn_meta,& focus.dzn_meta},{[this]{port.check_bindings();}}}
, dzn_locator(locator.clone().set(dzn_rt).set(dzn_pump))


, optics(dzn_locator)
, shutter(dzn_locator)
, focus(dzn_locator)

, port(optics.port)

, dzn_pump()
{
  optics.port.meta.requires.port = "port";



  port.in.prepare = [&] () {
    return dzn::shell(dzn_pump, [ & ] {return optics.port.in.prepare();});
  };
  port.in.capture = [&] () {
    return dzn::shell(dzn_pump, [ & ] {return optics.port.in.capture();});
  };
  port.in.cancel = [&] () {
    return dzn::shell(dzn_pump, [ & ] {return optics.port.in.cancel();});
  };


  optics.port.out.ready = std::ref(port.out.ready);



  optics.dzn_meta.parent = &dzn_meta;
  optics.dzn_meta.name = "optics";
  shutter.dzn_meta.parent = &dzn_meta;
  shutter.dzn_meta.name = "shutter";
  focus.dzn_meta.parent = &dzn_meta;
  focus.dzn_meta.name = "focus";

  connect(shutter.port, optics.shutter);
  connect(focus.port, optics.focus);

}

void OpticsThread::check_bindings() const
{
  dzn::check_bindings(&dzn_meta);
}
void OpticsThread::dump_tree(std::ostream& os) const
{
  dzn::dump_tree(os, &dzn_meta);
}



//version: 2.9.1