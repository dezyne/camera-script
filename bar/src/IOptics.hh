#include <dzn/meta.hh>

namespace dzn {
  struct locator;
  struct runtime;
}



#include <iostream>
#include <map>


/********************************** INTERFACE *********************************/
#ifndef IOPTICS_HH
#define IOPTICS_HH



struct IOptics
{
#ifndef ENUM_IOptics_State
#define ENUM_IOptics_State 1


  struct State
  {
    enum type
    {
      Idle,Prepare,Ready
    };
  };


#endif // ENUM_IOptics_State

  struct
  {
    std::function< void()> prepare;
    std::function< void()> capture;
    std::function< void()> cancel;
  } in;

  struct
  {
    std::function< void()> ready;
  } out;

  dzn::port::meta meta;
  inline IOptics(const dzn::port::meta& m) : meta(m) {}

  void check_bindings() const
  {
    if (! in.prepare) throw dzn::binding_error(meta, "in.prepare");
    if (! in.capture) throw dzn::binding_error(meta, "in.capture");
    if (! in.cancel) throw dzn::binding_error(meta, "in.cancel");

    if (! out.ready) throw dzn::binding_error(meta, "out.ready");

  }
};

inline void connect (IOptics& provided, IOptics& required)
{
  provided.out = required.out;
  required.in = provided.in;
  provided.meta.requires = required.meta.requires;
  required.meta.provides = provided.meta.provides;
}


#ifndef ENUM_TO_STRING_IOptics_State
#define ENUM_TO_STRING_IOptics_State 1
inline std::string to_string(::IOptics::State::type v)
{
  switch(v)
  {
    case ::IOptics::State::Idle: return "State_Idle";
    case ::IOptics::State::Prepare: return "State_Prepare";
    case ::IOptics::State::Ready: return "State_Ready";

  }
  return "";
}
#endif // ENUM_TO_STRING_IOptics_State

#ifndef STRING_TO_ENUM_IOptics_State
#define STRING_TO_ENUM_IOptics_State 1
inline ::IOptics::State::type to_IOptics_State(std::string s)
{
  static std::map<std::string, ::IOptics::State::type> m = {
    {"State_Idle", ::IOptics::State::Idle},
    {"State_Prepare", ::IOptics::State::Prepare},
    {"State_Ready", ::IOptics::State::Ready},
  };
  return m.at(s);
}
#endif // STRING_TO_ENUM_IOptics_State


#endif // IOPTICS_HH

/********************************** INTERFACE *********************************/


//version: 2.9.1
