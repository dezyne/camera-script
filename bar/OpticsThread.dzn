import IOptics.dzn;

interface ILens
{
  in void forward();
  in void backward();
  in void stop();

  out void stopped();

  behaviour
  {
    bool moving = false;

    [!moving] {
      on forward: {moving = true;}
      on backward: {moving = true;}
    }
    [moving] {
      on forward: illegal;
      on backward: illegal;
      on inevitable: {moving = false; stopped;}
    }
    on stop: moving = false;
  }
}
component Lens
{
  provides ILens port;
}
interface IFocus
{
  in void measure();
  in void cancel();
  out void maximum();

  behaviour
  {
    bool idle = true;
    [idle] on measure: idle = false;
    [!idle] on measure: illegal;
    on cancel: idle = true;
    [!idle] on optional: {idle = true; maximum;}
  }
}
interface IContrast
{
  enum EContrast {Sharper, Blurrier};
  in EContrast measure();

  behaviour
  {
    on measure: reply(EContrast.Sharper);
    on measure: reply(EContrast.Blurrier);
  }
}
component Contrast
{
  provides IContrast port;
}

component Focus
{
  provides IFocus port;
  system {
    FocusControl focus;
    Contrast sensor;
    Lens lens;

    port <=> focus.port;
    focus.lens <=> lens.port;
    focus.sensor <=> sensor.port;
  }
}

component FocusControl
{
  provides IFocus port;

  requires ILens lens;
  requires IContrast sensor;

  behaviour
  {
    bool idle = true;
    IContrast.EContrast contrast_was = IContrast.EContrast.Blurrier;
    [idle] on port.measure(): {idle = false; contrast_was = sensor.measure(); lens.forward();}
    on port.cancel(): {idle = true; lens.stop();}
    [!idle] on lens.stopped(): {
      IContrast.EContrast contrast_is = sensor.measure();
      if(contrast_was.Sharper && contrast_is.Blurrier) {idle = true; port.maximum();}
      else if(contrast_was.Sharper && contrast_is.Sharper) lens.forward();
      else lens.backward();
      contrast_was = contrast_is;
    }
  }
}

interface IShutter
{
  in void expose();
  behaviour
  {
    on expose: {}
  }
}
component Shutter
{
  provides IShutter port;
}

component OpticsControl
{
  provides IOptics port;

  requires IFocus focus;
  requires IShutter shutter;

  behaviour
  {
    enum State {Idle, Prepare, Ready};
    State state = State.Idle;
    bool ready = false;
    on port.prepare():
    {
      [state.Idle] {state = State.Prepare; focus.measure();}
    }
    on port.cancel():
    {
      focus.cancel();
      state = State.Idle;
    }
    [state.Prepare] on focus.maximum(): {state = State.Ready; port.ready();}
    on port.capture():
    {
      [state.Prepare] {state = State.Idle; focus.cancel(); shutter.expose();}
      [state.Ready]   {state = State.Idle; shutter.expose();}
    }
  }
}
component OpticsThread
{
  provides IOptics port;

  system
  {
    OpticsControl optics;

    Shutter shutter;
    Focus focus;

    port <=> optics.port;

    optics.shutter <=> shutter.port;

    optics.focus <=> focus.port;
  }
}
