#ifndef SHUTTER_HH
#define SHUTTER_HH

struct Shutter: public skel::Shutter
{
  Shutter(const dzn::locator& l)
  : skel::Shutter(l)
  {}
  virtual void port_expose () {std::clog << "expose\n";}
};

#endif // SHUTTER_HH
