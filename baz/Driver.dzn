import IOptics.dzn;
import IAcquisition.dzn;

interface IControl
{
  in void setup();
  in void shoot();
  in void release();

  out void focus_lock();
  out void image();

  behaviour
  {
    enum State {Idle, Setup, Ready, Acquire};
    State state = State.Idle;

    [state.Idle] {
      on setup:{ state = State.Setup; }
      on shoot,
      release: illegal;
    }
    [state.Setup] {
      on setup: illegal;
      on shoot: { state = State.Acquire; }
      on release: { state = State.Idle; }
      on optional: { state = State.Ready; focus_lock; }
    }
    [state.Ready] {
      on setup: illegal;
      on release: { state = State.Idle; }
      on shoot: { state = State.Acquire; }
    }
    [state.Acquire] {
      on setup,
      release,
      shoot: illegal;
      on inevitable: { image; state = State.Idle; }
    }
  }
}
component Driver
{
  provides IControl control;
  requires IAcquisition acquisition;
  requires IOptics optics;

  behaviour
  {
    enum State {Idle, Setup, Ready, Acquire};

    State state = State.Idle;

    [state.Idle]
    {
      on control.setup(): {optics.prepare(); acquisition.prepare(); state = State.Setup;}
      on optics.ready(): {}
    }
    [state.Setup]
    {
      on optics.ready(): {state = State.Ready; control.focus_lock();}
      on control.shoot(): {acquisition.acquire(); optics.capture(); state = State.Acquire;}
      on control.release(): {acquisition.cancel(); optics.cancel(); state = State.Idle;}
    }
    [state.Ready]
    {
      on control.shoot(): {acquisition.acquire(); optics.capture(); state = State.Acquire;}
      on control.release(): {acquisition.cancel(); optics.cancel(); state = State.Idle;}
    }
    [state.Acquire]
    {
      on acquisition.image(): {control.image(); state = State.Idle;}
      on optics.ready(): {}
    }
  }
}
