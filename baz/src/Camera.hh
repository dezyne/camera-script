#include <dzn/meta.hh>

namespace dzn {
  struct locator;
  struct runtime;
}



#include <iostream>
#include <map>


/***********************************  FOREIGN  **********************************/
#ifndef SKEL_ACQUISITIONTHREAD_HH
#define SKEL_ACQUISITIONTHREAD_HH

#include <dzn/locator.hh>
#include <dzn/runtime.hh>

#include "IAcquisition.hh"



namespace skel {
  struct AcquisitionThread
  {
    dzn::meta dzn_meta;
    dzn::runtime& dzn_rt;
    dzn::locator const& dzn_locator;
    ::IAcquisition port;


    AcquisitionThread(const dzn::locator& dzn_locator)
    : dzn_meta{"","AcquisitionThread",0,0,{},{},{[this]{port.check_bindings();}}}
    , dzn_rt(dzn_locator.get<dzn::runtime>())
    , dzn_locator(dzn_locator)

    , port({{"port",this,&dzn_meta},{"",0,0}})


    {
      port.in.prepare = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_prepare();}, this->port.meta, "prepare");};
      port.in.cancel = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_cancel();}, this->port.meta, "cancel");};
      port.in.acquire = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_acquire();}, this->port.meta, "acquire");};


    }
    virtual ~ AcquisitionThread() {}
    virtual std::ostream& stream_members(std::ostream& os) const { return os; }
    void check_bindings() const;
    void dump_tree(std::ostream& os) const;
    void set_state(std::map<std::string,std::map<std::string,std::string> >){}
    void set_state(std::map<std::string,std::string>_alist){}
    friend std::ostream& operator << (std::ostream& os, const AcquisitionThread& m)  {
      return m.stream_members(os);
    }
    private:
    virtual void port_prepare () = 0;
    virtual void port_cancel () = 0;
    virtual void port_acquire () = 0;

  };
}

#endif // ACQUISITIONTHREAD_HH

/***********************************  FOREIGN  **********************************/
/***********************************  FOREIGN  **********************************/
#ifndef SKEL_OPTICSTHREAD_HH
#define SKEL_OPTICSTHREAD_HH

#include <dzn/locator.hh>
#include <dzn/runtime.hh>

#include "IOptics.hh"



namespace skel {
  struct OpticsThread
  {
    dzn::meta dzn_meta;
    dzn::runtime& dzn_rt;
    dzn::locator const& dzn_locator;
    ::IOptics port;


    OpticsThread(const dzn::locator& dzn_locator)
    : dzn_meta{"","OpticsThread",0,0,{},{},{[this]{port.check_bindings();}}}
    , dzn_rt(dzn_locator.get<dzn::runtime>())
    , dzn_locator(dzn_locator)

    , port({{"port",this,&dzn_meta},{"",0,0}})


    {
      port.in.prepare = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_prepare();}, this->port.meta, "prepare");};
      port.in.capture = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_capture();}, this->port.meta, "capture");};
      port.in.cancel = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_cancel();}, this->port.meta, "cancel");};


    }
    virtual ~ OpticsThread() {}
    virtual std::ostream& stream_members(std::ostream& os) const { return os; }
    void check_bindings() const;
    void dump_tree(std::ostream& os) const;
    void set_state(std::map<std::string,std::map<std::string,std::string> >){}
    void set_state(std::map<std::string,std::string>_alist){}
    friend std::ostream& operator << (std::ostream& os, const OpticsThread& m)  {
      return m.stream_members(os);
    }
    private:
    virtual void port_prepare () = 0;
    virtual void port_capture () = 0;
    virtual void port_cancel () = 0;

  };
}

#endif // OPTICSTHREAD_HH

/***********************************  FOREIGN  **********************************/
/***********************************  SYSTEM  ***********************************/
#ifndef CAMERA_HH
#define CAMERA_HH


#include <dzn/locator.hh>

#include "Driver.hh"
#include "AcquisitionThread.hh"
#include "OpticsThread.hh"



struct Camera
{
  dzn::meta dzn_meta;
  dzn::runtime& dzn_rt;
  dzn::locator const& dzn_locator;


  ::Driver driver;
  ::AcquisitionThread acquisition;
  ::OpticsThread optics;

  ::IControl& control;


  Camera(const dzn::locator&);
  void check_bindings() const;
  void dump_tree(std::ostream& os=std::clog) const;
};

#endif // CAMERA_HH

/***********************************  SYSTEM  ***********************************/


//version: 2.9.1
