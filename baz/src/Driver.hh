#include <dzn/meta.hh>

namespace dzn {
  struct locator;
  struct runtime;
}



#include <iostream>
#include <map>


/********************************** INTERFACE *********************************/
#ifndef ICONTROL_HH
#define ICONTROL_HH



struct IControl
{
#ifndef ENUM_IControl_State
#define ENUM_IControl_State 1


  struct State
  {
    enum type
    {
      Idle,Setup,Ready,Acquire
    };
  };


#endif // ENUM_IControl_State

  struct
  {
    std::function< void()> setup;
    std::function< void()> shoot;
    std::function< void()> release;
  } in;

  struct
  {
    std::function< void()> focus_lock;
    std::function< void()> image;
  } out;

  dzn::port::meta meta;
  inline IControl(const dzn::port::meta& m) : meta(m) {}

  void check_bindings() const
  {
    if (! in.setup) throw dzn::binding_error(meta, "in.setup");
    if (! in.shoot) throw dzn::binding_error(meta, "in.shoot");
    if (! in.release) throw dzn::binding_error(meta, "in.release");

    if (! out.focus_lock) throw dzn::binding_error(meta, "out.focus_lock");
    if (! out.image) throw dzn::binding_error(meta, "out.image");

  }
};

inline void connect (IControl& provided, IControl& required)
{
  provided.out = required.out;
  required.in = provided.in;
  provided.meta.requires = required.meta.requires;
  required.meta.provides = provided.meta.provides;
}


#ifndef ENUM_TO_STRING_IControl_State
#define ENUM_TO_STRING_IControl_State 1
inline std::string to_string(::IControl::State::type v)
{
  switch(v)
  {
    case ::IControl::State::Idle: return "State_Idle";
    case ::IControl::State::Setup: return "State_Setup";
    case ::IControl::State::Ready: return "State_Ready";
    case ::IControl::State::Acquire: return "State_Acquire";

  }
  return "";
}
#endif // ENUM_TO_STRING_IControl_State

#ifndef STRING_TO_ENUM_IControl_State
#define STRING_TO_ENUM_IControl_State 1
inline ::IControl::State::type to_IControl_State(std::string s)
{
  static std::map<std::string, ::IControl::State::type> m = {
    {"State_Idle", ::IControl::State::Idle},
    {"State_Setup", ::IControl::State::Setup},
    {"State_Ready", ::IControl::State::Ready},
    {"State_Acquire", ::IControl::State::Acquire},
  };
  return m.at(s);
}
#endif // STRING_TO_ENUM_IControl_State


#endif // ICONTROL_HH

/********************************** INTERFACE *********************************/
/********************************** COMPONENT *********************************/
#ifndef DRIVER_HH
#define DRIVER_HH

#include "IAcquisition.hh"
#include "IOptics.hh"



struct Driver
{
  dzn::meta dzn_meta;
  dzn::runtime& dzn_rt;
  dzn::locator const& dzn_locator;
#ifndef ENUM_Driver_State
#define ENUM_Driver_State 1


  struct State
  {
    enum type
    {
      Idle,Setup,Ready,Acquire
    };
  };


#endif // ENUM_Driver_State

  ::Driver::State::type state;


  std::function<void ()> out_control;

  ::IControl control;

  ::IAcquisition acquisition;
  ::IOptics optics;


  Driver(const dzn::locator&);
  void check_bindings() const;
  void dump_tree(std::ostream& os) const;
  friend std::ostream& operator << (std::ostream& os, const Driver& m)  {
    (void)m;
    return os << "[" << m.state <<"]" ;
  }
  private:
  void control_setup();
  void control_shoot();
  void control_release();
  void acquisition_image();
  void acquisition_focus();
  void optics_ready();

};

#endif // DRIVER_HH

/********************************** COMPONENT *********************************/


//version: 2.9.1
