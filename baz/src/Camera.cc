#include "Camera.hh"

#include <dzn/locator.hh>
#include <dzn/runtime.hh>


//SYSTEM

Camera::Camera(const dzn::locator& dzn_locator)
: dzn_meta{"","Camera",0,0,{},{& driver.dzn_meta,& acquisition.dzn_meta,& optics.dzn_meta},{[this]{control.check_bindings();}}}
, dzn_rt(dzn_locator.get<dzn::runtime>())
, dzn_locator(dzn_locator)


, driver(dzn_locator)
, acquisition(dzn_locator)
, optics(dzn_locator)

, control(driver.control)

{


  driver.dzn_meta.parent = &dzn_meta;
  driver.dzn_meta.name = "driver";
  acquisition.dzn_meta.parent = &dzn_meta;
  acquisition.dzn_meta.name = "acquisition";
  optics.dzn_meta.parent = &dzn_meta;
  optics.dzn_meta.name = "optics";


  connect(acquisition.port, driver.acquisition);
  connect(optics.port, driver.optics);

  dzn::rank(control.meta.provides.meta, 0);

}

void Camera::check_bindings() const
{
  dzn::check_bindings(&dzn_meta);
}
void Camera::dump_tree(std::ostream& os) const
{
  dzn::dump_tree(os, &dzn_meta);
}

////////////////////////////////////////////////////////////////////////////////



//version: 2.9.1