#include "Driver.hh"

#include <dzn/locator.hh>
#include <dzn/runtime.hh>



Driver::Driver(const dzn::locator& dzn_locator)
: dzn_meta{"","Driver",0,0,{& acquisition.meta,& optics.meta},{},{[this]{control.check_bindings();},[this]{acquisition.check_bindings();},[this]{optics.check_bindings();}}}
, dzn_rt(dzn_locator.get<dzn::runtime>())
, dzn_locator(dzn_locator)
, state(::Driver::State::Idle)

, control({{"control",this,&dzn_meta},{"",0,0}})

, acquisition({{"",0,0},{"acquisition",this,&dzn_meta}})
, optics({{"",0,0},{"optics",this,&dzn_meta}})


{
  dzn_rt.performs_flush(this) = true;

  control.in.setup = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->control) = false; return control_setup();}, this->control.meta, "setup");};
  control.in.shoot = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->control) = false; return control_shoot();}, this->control.meta, "shoot");};
  control.in.release = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->control) = false; return control_release();}, this->control.meta, "release");};
  acquisition.out.image = [&](){return dzn::call_out(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->acquisition) = false; return acquisition_image();}, this->acquisition.meta, "image");};
  acquisition.out.focus = [&](){return dzn::call_out(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->acquisition) = false; return acquisition_focus();}, this->acquisition.meta, "focus");};
  optics.out.ready = [&](){return dzn::call_out(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->optics) = false; return optics_ready();}, this->optics.meta, "ready");};





}

void Driver::control_setup()
{
  if (state == ::Driver::State::Idle) 
  {
    this->optics.in.prepare();
    this->acquisition.in.prepare();
    state = ::Driver::State::Setup;
  }
  else if (!(state == ::Driver::State::Idle)) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void Driver::control_shoot()
{
  if (state == ::Driver::State::Setup) 
  {
    this->acquisition.in.acquire();
    this->optics.in.capture();
    state = ::Driver::State::Acquire;
  }
  else if (state == ::Driver::State::Ready) 
  {
    this->acquisition.in.acquire();
    this->optics.in.capture();
    state = ::Driver::State::Acquire;
  }
  else if ((!(state == ::Driver::State::Ready) && !(state == ::Driver::State::Setup))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void Driver::control_release()
{
  if (state == ::Driver::State::Setup) 
  {
    this->acquisition.in.cancel();
    this->optics.in.cancel();
    state = ::Driver::State::Idle;
  }
  else if (state == ::Driver::State::Ready) 
  {
    this->acquisition.in.cancel();
    this->optics.in.cancel();
    state = ::Driver::State::Idle;
  }
  else if ((!(state == ::Driver::State::Ready) && !(state == ::Driver::State::Setup))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void Driver::acquisition_image()
{
  if (state == ::Driver::State::Acquire) 
  {
    this->control.out.image();
    state = ::Driver::State::Idle;
  }
  else if (!(state == ::Driver::State::Acquire)) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void Driver::acquisition_focus()
{
  dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void Driver::optics_ready()
{
  if (state == ::Driver::State::Idle) ;
  else if (state == ::Driver::State::Setup) 
  {
    state = ::Driver::State::Ready;
    this->control.out.focus_lock();
  }
  else if (state == ::Driver::State::Acquire) ;
  else if ((!(state == ::Driver::State::Acquire) && (!(state == ::Driver::State::Setup) && !(state == ::Driver::State::Idle)))) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}


void Driver::check_bindings() const
{
  dzn::check_bindings(&dzn_meta);
}
void Driver::dump_tree(std::ostream& os) const
{
  dzn::dump_tree(os, &dzn_meta);
}




//version: 2.9.1