#include <dzn/meta.hh>

namespace dzn {
  struct locator;
  struct runtime;
}



#include <iostream>
#include <map>


/********************************** INTERFACE *********************************/
#ifndef ISENSOR_HH
#define ISENSOR_HH



struct ISensor
{
#ifndef ENUM_ISensor_State
#define ENUM_ISensor_State 1


  struct State
  {
    enum type
    {
      Idle,Ready,Acquire
    };
  };


#endif // ENUM_ISensor_State

  struct
  {
    std::function< void()> prepare;
    std::function< void()> acquire;
    std::function< void()> cancel;
  } in;

  struct
  {
    std::function< void()> image;
  } out;

  dzn::port::meta meta;
  inline ISensor(const dzn::port::meta& m) : meta(m) {}

  void check_bindings() const
  {
    if (! in.prepare) throw dzn::binding_error(meta, "in.prepare");
    if (! in.acquire) throw dzn::binding_error(meta, "in.acquire");
    if (! in.cancel) throw dzn::binding_error(meta, "in.cancel");

    if (! out.image) throw dzn::binding_error(meta, "out.image");

  }
};

inline void connect (ISensor& provided, ISensor& required)
{
  provided.out = required.out;
  required.in = provided.in;
  provided.meta.requires = required.meta.requires;
  required.meta.provides = provided.meta.provides;
}


#ifndef ENUM_TO_STRING_ISensor_State
#define ENUM_TO_STRING_ISensor_State 1
inline std::string to_string(::ISensor::State::type v)
{
  switch(v)
  {
    case ::ISensor::State::Idle: return "State_Idle";
    case ::ISensor::State::Ready: return "State_Ready";
    case ::ISensor::State::Acquire: return "State_Acquire";

  }
  return "";
}
#endif // ENUM_TO_STRING_ISensor_State

#ifndef STRING_TO_ENUM_ISensor_State
#define STRING_TO_ENUM_ISensor_State 1
inline ::ISensor::State::type to_ISensor_State(std::string s)
{
  static std::map<std::string, ::ISensor::State::type> m = {
    {"State_Idle", ::ISensor::State::Idle},
    {"State_Ready", ::ISensor::State::Ready},
    {"State_Acquire", ::ISensor::State::Acquire},
  };
  return m.at(s);
}
#endif // STRING_TO_ENUM_ISensor_State


#endif // ISENSOR_HH

/********************************** INTERFACE *********************************/
/********************************** INTERFACE *********************************/
#ifndef IMEMORY_HH
#define IMEMORY_HH



struct IMemory
{

  struct
  {
    std::function< void()> store;
  } in;

  struct
  {
  } out;

  dzn::port::meta meta;
  inline IMemory(const dzn::port::meta& m) : meta(m) {}

  void check_bindings() const
  {
    if (! in.store) throw dzn::binding_error(meta, "in.store");


  }
};

inline void connect (IMemory& provided, IMemory& required)
{
  provided.out = required.out;
  required.in = provided.in;
  provided.meta.requires = required.meta.requires;
  required.meta.provides = provided.meta.provides;
}





#endif // IMEMORY_HH

/********************************** INTERFACE *********************************/
/***********************************  FOREIGN  **********************************/
#ifndef SKEL_CMOS_HH
#define SKEL_CMOS_HH

#include <dzn/locator.hh>
#include <dzn/runtime.hh>




namespace skel {
  struct CMOS
  {
    dzn::meta dzn_meta;
    dzn::runtime& dzn_rt;
    dzn::locator const& dzn_locator;
    ::ISensor port;


    CMOS(const dzn::locator& dzn_locator)
    : dzn_meta{"","CMOS",0,0,{},{},{[this]{port.check_bindings();}}}
    , dzn_rt(dzn_locator.get<dzn::runtime>())
    , dzn_locator(dzn_locator)

    , port({{"port",this,&dzn_meta},{"",0,0}})


    {
      port.in.prepare = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_prepare();}, this->port.meta, "prepare");};
      port.in.acquire = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_acquire();}, this->port.meta, "acquire");};
      port.in.cancel = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_cancel();}, this->port.meta, "cancel");};


    }
    virtual ~ CMOS() {}
    virtual std::ostream& stream_members(std::ostream& os) const { return os; }
    void check_bindings() const;
    void dump_tree(std::ostream& os) const;
    void set_state(std::map<std::string,std::map<std::string,std::string> >){}
    void set_state(std::map<std::string,std::string>_alist){}
    friend std::ostream& operator << (std::ostream& os, const CMOS& m)  {
      return m.stream_members(os);
    }
    private:
    virtual void port_prepare () = 0;
    virtual void port_acquire () = 0;
    virtual void port_cancel () = 0;

  };
}

#endif // CMOS_HH

/***********************************  FOREIGN  **********************************/
/***********************************  FOREIGN  **********************************/
#ifndef SKEL_MEMORY_HH
#define SKEL_MEMORY_HH

#include <dzn/locator.hh>
#include <dzn/runtime.hh>




namespace skel {
  struct Memory
  {
    dzn::meta dzn_meta;
    dzn::runtime& dzn_rt;
    dzn::locator const& dzn_locator;
    ::IMemory port;


    Memory(const dzn::locator& dzn_locator)
    : dzn_meta{"","Memory",0,0,{},{},{[this]{port.check_bindings();}}}
    , dzn_rt(dzn_locator.get<dzn::runtime>())
    , dzn_locator(dzn_locator)

    , port({{"port",this,&dzn_meta},{"",0,0}})


    {
      port.in.store = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_store();}, this->port.meta, "store");};


    }
    virtual ~ Memory() {}
    virtual std::ostream& stream_members(std::ostream& os) const { return os; }
    void check_bindings() const;
    void dump_tree(std::ostream& os) const;
    void set_state(std::map<std::string,std::map<std::string,std::string> >){}
    void set_state(std::map<std::string,std::string>_alist){}
    friend std::ostream& operator << (std::ostream& os, const Memory& m)  {
      return m.stream_members(os);
    }
    private:
    virtual void port_store () = 0;

  };
}

#endif // MEMORY_HH

/***********************************  FOREIGN  **********************************/
/********************************** COMPONENT *********************************/
#ifndef ACQUIRE_HH
#define ACQUIRE_HH

#include "IAcquisition.hh"



struct Acquire
{
  dzn::meta dzn_meta;
  dzn::runtime& dzn_rt;
  dzn::locator const& dzn_locator;
#ifndef ENUM_Acquire_State
#define ENUM_Acquire_State 1


  struct State
  {
    enum type
    {
      Idle,Ready,Acquiring
    };
  };


#endif // ENUM_Acquire_State

  ::Acquire::State::type state;


  std::function<void ()> out_port;

  ::IAcquisition port;

  ::IMemory memory;
  ::ISensor sensor;


  Acquire(const dzn::locator&);
  void check_bindings() const;
  void dump_tree(std::ostream& os) const;
  friend std::ostream& operator << (std::ostream& os, const Acquire& m)  {
    (void)m;
    return os << "[" << m.state <<"]" ;
  }
  private:
  void port_prepare();
  void port_cancel();
  void port_acquire();
  void sensor_image();

};

#endif // ACQUIRE_HH

/********************************** COMPONENT *********************************/
/***********************************  SHELL  ************************************/
#ifndef ACQUISITIONTHREAD_HH
#define ACQUISITIONTHREAD_HH


#include <dzn/locator.hh>
#include <dzn/runtime.hh>
#include <dzn/pump.hh>

#include "Memory.hh"
#include "CMOS.hh"



struct AcquisitionThread
{
  dzn::meta dzn_meta;
  dzn::runtime dzn_rt;
  dzn::locator dzn_locator;


  ::Acquire acquire;
  ::Memory memory;
  ::CMOS sensor;

  ::IAcquisition port;


  dzn::pump dzn_pump;
  AcquisitionThread(const dzn::locator&);
  void check_bindings() const;
  void dump_tree(std::ostream& os=std::clog) const;
};

#endif // ACQUISITIONTHREAD_HH

/***********************************  SHELL  ************************************/


//version: 2.9.1
