#include <dzn/meta.hh>

namespace dzn {
  struct locator;
  struct runtime;
}



#include <iostream>
#include <map>


/********************************** INTERFACE *********************************/
#ifndef IACQUISITION_HH
#define IACQUISITION_HH



struct IAcquisition
{
#ifndef ENUM_IAcquisition_State
#define ENUM_IAcquisition_State 1


  struct State
  {
    enum type
    {
      Idle,Ready,Acquiring
    };
  };


#endif // ENUM_IAcquisition_State

  struct
  {
    std::function< void()> prepare;
    std::function< void()> cancel;
    std::function< void()> acquire;
  } in;

  struct
  {
    std::function< void()> image;
    std::function< void()> focus;
  } out;

  dzn::port::meta meta;
  inline IAcquisition(const dzn::port::meta& m) : meta(m) {}

  void check_bindings() const
  {
    if (! in.prepare) throw dzn::binding_error(meta, "in.prepare");
    if (! in.cancel) throw dzn::binding_error(meta, "in.cancel");
    if (! in.acquire) throw dzn::binding_error(meta, "in.acquire");

    if (! out.image) throw dzn::binding_error(meta, "out.image");
    if (! out.focus) throw dzn::binding_error(meta, "out.focus");

  }
};

inline void connect (IAcquisition& provided, IAcquisition& required)
{
  provided.out = required.out;
  required.in = provided.in;
  provided.meta.requires = required.meta.requires;
  required.meta.provides = provided.meta.provides;
}


#ifndef ENUM_TO_STRING_IAcquisition_State
#define ENUM_TO_STRING_IAcquisition_State 1
inline std::string to_string(::IAcquisition::State::type v)
{
  switch(v)
  {
    case ::IAcquisition::State::Idle: return "State_Idle";
    case ::IAcquisition::State::Ready: return "State_Ready";
    case ::IAcquisition::State::Acquiring: return "State_Acquiring";

  }
  return "";
}
#endif // ENUM_TO_STRING_IAcquisition_State

#ifndef STRING_TO_ENUM_IAcquisition_State
#define STRING_TO_ENUM_IAcquisition_State 1
inline ::IAcquisition::State::type to_IAcquisition_State(std::string s)
{
  static std::map<std::string, ::IAcquisition::State::type> m = {
    {"State_Idle", ::IAcquisition::State::Idle},
    {"State_Ready", ::IAcquisition::State::Ready},
    {"State_Acquiring", ::IAcquisition::State::Acquiring},
  };
  return m.at(s);
}
#endif // STRING_TO_ENUM_IAcquisition_State


#endif // IACQUISITION_HH

/********************************** INTERFACE *********************************/


//version: 2.9.1
