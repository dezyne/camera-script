#include "AcquisitionThread.hh"

#include <dzn/locator.hh>
#include <dzn/runtime.hh>



Acquire::Acquire(const dzn::locator& dzn_locator)
: dzn_meta{"","Acquire",0,0,{& memory.meta,& sensor.meta},{},{[this]{port.check_bindings();},[this]{memory.check_bindings();},[this]{sensor.check_bindings();}}}
, dzn_rt(dzn_locator.get<dzn::runtime>())
, dzn_locator(dzn_locator)
, state(::Acquire::State::Idle)

, port({{"port",this,&dzn_meta},{"",0,0}})

, memory({{"",0,0},{"memory",this,&dzn_meta}})
, sensor({{"",0,0},{"sensor",this,&dzn_meta}})


{
  dzn_rt.performs_flush(this) = true;

  port.in.prepare = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_prepare();}, this->port.meta, "prepare");};
  port.in.cancel = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_cancel();}, this->port.meta, "cancel");};
  port.in.acquire = [&](){return dzn::call_in(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->port) = false; return port_acquire();}, this->port.meta, "acquire");};
  sensor.out.image = [&](){return dzn::call_out(this,[=]{ dzn_locator.get<dzn::runtime>().skip_block(&this->sensor) = false; return sensor_image();}, this->sensor.meta, "image");};





}

void Acquire::port_prepare()
{
  if (state == ::Acquire::State::Idle) 
  {
    this->sensor.in.prepare();
    state = ::Acquire::State::Ready;
  }
  else if (!(state == ::Acquire::State::Idle)) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void Acquire::port_cancel()
{

  {
    state = ::Acquire::State::Idle;
    this->sensor.in.cancel();
  }

  return;

}
void Acquire::port_acquire()
{
  if (state == ::Acquire::State::Ready) 
  {
    this->sensor.in.acquire();
    state = ::Acquire::State::Acquiring;
  }
  else if (!(state == ::Acquire::State::Ready)) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}
void Acquire::sensor_image()
{
  if (state == ::Acquire::State::Acquiring) 
  {
    this->memory.in.store();
    this->port.out.image();
    state = ::Acquire::State::Idle;
  }
  else if (!(state == ::Acquire::State::Acquiring)) dzn_locator.get<dzn::illegal_handler>().illegal();
  else dzn_locator.get<dzn::illegal_handler>().illegal();

  return;

}


void Acquire::check_bindings() const
{
  dzn::check_bindings(&dzn_meta);
}
void Acquire::dump_tree(std::ostream& os) const
{
  dzn::dump_tree(os, &dzn_meta);
}


AcquisitionThread::AcquisitionThread(const dzn::locator& locator)
: dzn_meta{"","AcquisitionThread",0,0,{},{& acquire.dzn_meta,& memory.dzn_meta,& sensor.dzn_meta},{[this]{port.check_bindings();}}}
, dzn_locator(locator.clone().set(dzn_rt).set(dzn_pump))


, acquire(dzn_locator)
, memory(dzn_locator)
, sensor(dzn_locator)

, port(acquire.port)

, dzn_pump()
{
  acquire.port.meta.requires.port = "port";



  port.in.prepare = [&] () {
    return dzn::shell(dzn_pump, [ & ] {return acquire.port.in.prepare();});
  };
  port.in.cancel = [&] () {
    return dzn::shell(dzn_pump, [ & ] {return acquire.port.in.cancel();});
  };
  port.in.acquire = [&] () {
    return dzn::shell(dzn_pump, [ & ] {return acquire.port.in.acquire();});
  };


  acquire.port.out.image = std::ref(port.out.image);
  acquire.port.out.focus = std::ref(port.out.focus);



  acquire.dzn_meta.parent = &dzn_meta;
  acquire.dzn_meta.name = "acquire";
  memory.dzn_meta.parent = &dzn_meta;
  memory.dzn_meta.name = "memory";
  sensor.dzn_meta.parent = &dzn_meta;
  sensor.dzn_meta.name = "sensor";

  connect(memory.port, acquire.memory);
  connect(sensor.port, acquire.sensor);

}

void AcquisitionThread::check_bindings() const
{
  dzn::check_bindings(&dzn_meta);
}
void AcquisitionThread::dump_tree(std::ostream& os) const
{
  dzn::dump_tree(os, &dzn_meta);
}



//version: 2.9.1