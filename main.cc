#include <dzn/runtime.hh>
#include <dzn/locator.hh>

#include "Camera.hh"

int main()
{
  // create runtime infrastructure
  dzn::locator locator;
  dzn::runtime runtime;
  dzn::illegal_handler illegal_handler;

  // create camera component
  Camera cam(locator.set(runtime).set(illegal_handler));
  cam.dzn_meta.name = "camera";

  // stub unconnected callback functions from camera component
  cam.control.out.focus_lock = []{std::cout << "Driver.control.focus_lock" << std::endl;};
  cam.control.out.image = []{std::cout << "Driver.control.image" << std::endl;};

  // play the example test trace
  cam.control.in.setup();
  cam.control.in.shoot();
}
