#! /usr/bin/env bash

set -ex

# customize these for you environment:
DZN=~/.npm/bin/dzn
MODELDIRS="foo bar baz"
DZN_INCLUDES="-I foo -I bar -I baz"

# setup
if ! node --version; then
    echo "please install nodejs in PATH"
    exit 1
fi

if ! $DZN --version; then
    echo "please install dzn in PATH"
    echo "npm install -g https://dezyne.verum.com/download/npm/dzn-2.9.1.tar.gz"
    exit 1
fi

if ! $DZN hello; then
    echo "please authenticate dzn"
    exit 1
fi

#clean
rm -fr foo/src bar/src baz/src build runtime

RUNTIME_HH=$($DZN ls /share/runtime/c++/dzn)
RUNTIME_CC=$($DZN ls /share/runtime/c++|grep .cc)

# runtime
mkdir -p runtime/dzn
for i in $RUNTIME_HH; do
    $DZN cat /share/runtime/c++/dzn/$i > runtime/dzn/$i
done
for i in $RUNTIME_CC; do
    $DZN cat /share/runtime/c++/$i > runtime/$i
done

# code
for dir in $MODELDIRS; do
    for file in $dir/*.dzn; do
        $DZN code $DZN_INCLUDES -o $dir/src $file
    done
done

THREAD_SAFE_SHELLS="foo/AcquisitionThread.dzn bar/OpticsThread.dzn"
for file in $THREAD_SAFE_SHELLS; do
    $DZN code --shell=$(basename $file .dzn) -o $(dirname $file)/src/ $file
done
