#ifndef LENS_HH
#define LENS_HH

struct Lens: public skel::Lens
{
  Lens(const dzn::locator& l)
  : skel::Lens(l)
  {}
  virtual void port_forward () {std::clog << "forward\n";}
  virtual void port_backward () {std::clog << "backward\n";}
  virtual void port_stop () {std::clog << "stop\n";}
};

#endif // LENS_HH
